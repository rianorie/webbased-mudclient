<?php
	set_time_limit(0);
	header("Cache-Control: no-cache, must-revalidate");
	header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
    header('Content-Encoding', 'chunked');
    header('Transfer-Encoding', 'chunked');
    header('Content-Type', 'text/html');
    header('Connection', 'keep-alive');

    if (ob_get_level() == 0) { ob_start(); }
    echo str_pad('',1024)."\n";
    ob_flush();
    flush();
?>
<!DOCTYPE html>
<html>
<head>
    <title>webproxy</title>
</head>
<body>
<?php
class TelnetProxy
{
    const INPUT_FILE = 1;
    const INPUT_DB = 1;

    private $_id;

    private $_inputtype;

    private $telnetHost;
    private $telnetPort;

    private $_socket;
    private $_state;

    private $dbName;
    private $dbHost;
    private $dbPort;
    private $dbUser;
    private $dbPass;

    private $_db;

    public function setTelnetInfo($strHost, $intPort)
    {
        $this->telnetHost = $strHost;
        $this->telnetPort = $intPort;
    }

    public function setDbInfo($strHost, $strUser, $strPassword, $strDb, $intPort = null)
    {
        $this->dbHost = $strHost;
        $this->dbPort = $intPort;
        $this->dbUser = $strUser;
        $this->dbPass = $strPassword;

        $this->dbName = $strDb;

    }

    public function run()
    {
        // Setup the db connection
        if ($this->dbConnect()) {

            // Setup the telnet connection
            if ($this->telnetConnect()) {

                $this->_id = $this->generateId();
                $this->webWriteId( $this->_id );
                $this->webWriteConnect();

                // and start the loop!
                while($this->telnetState()) {

                    $data = null;
                    if ($this->_inputtype == $this::INPUT_DB)   { $data = $this->dbGetInput(); }
                    if ($this->_inputtype == $this::INPUT_FILE) { $data = $this->fileGetInput(); }
//                    if ($data == '') { $data = "\n"; }
                    if ($data !== null) {
                        $this->webWriteDebug('Input at loop: '.str_replace("\n", "\\\n", $data));
                        fwrite($this->_socket, $data);
                    }

                    $strOut = '';
                    while($strBuffer = fread($this->_socket, (1024*10))) {
                        $strOut .= $strBuffer;
                    }

                    if ($strOut != '') {
                        $this->webWriteText($strOut);
                    }

                    usleep(100000); // 10th of a second
                }
                $this->webWriteDisconnect();

            } else {
                $this->webWriteError('Failed to connect to server.');
                $this->webWriteDebug('Host: '.$this->telnetHost.', port: '.$this->telnetPort);
            }
        } else {
            $this->webWriteError('Failed to connect to the database.');
        }
    }

    private function telnetConnect()
    {
        $this->_socket = fsockopen($this->telnetHost, $this->telnetPort);
        stream_set_blocking($this->_socket, 0);
        $this->_state = (is_resource($this->_socket));
        return $this->_state;
    }

    private function telnetState()
    {
        $meta = stream_get_meta_data($this->_socket);
        $this->_state = !$meta['eof'];
        return $this->_state;
    }

    private function telnetDisconnect()
    {
        fclose($this->_socket);
    }

    private function webWriteId($strTxt)
    {
        $this->webWrite('parent.mudclient.connection.adapters.webproxy.setId(\''.$strTxt.'\');');
    }

    private function webWriteConnect()
    {
        $this->webWrite('parent.mudclient.connection.adapters.webproxy.connected();');
    }

    private function webWriteDisconnect()
    {
        $this->webWrite('parent.mudclient.connection.adapters.webproxy.disconnected();');
    }

    private function webWriteText($strTxt)
    {
        $this->webWrite('parent.mudclient.connection.adapters.webproxy.receive(\''.base64_encode($strTxt).'\');');
    }

    private function webWriteError($strTxt)
    {
        $this->webWrite('parent.mudclient.connection.adapters.webproxy.error(\''.base64_encode($strTxt).'\');');
    }

    private function webWriteDebug($strTxt)
    {
        $this->webWrite('parent.mudclient.util.debugMsg(\''.$strTxt.'\');');
    }

    private function webWrite($strTxt) {

        echo "\r\n";
        echo '<script type="text/javascript">'.$strTxt.'</script>';
        echo "\r\n";
        ob_flush();
        flush();
    }

    public function setInputType($intType)
    {
        $this->_inputtype = $intType;
    }

    private function dbConnect()
    {
        try {
            $this->_db = new PDO('mysql:dbname='.$this->dbName.';host='.$this->dbHost.($this->dbPort == null ? '' : ':'.$this->dbPort), $this->dbUser, $this->dbPass);
            return true;

        } catch (PDOException $e) {
            $this->webWriteError('Database connection failed: ' . $e->getMessage());
            return false;
        }
    }

    private function dbGet()
    {
        return $this->_db;
    }

    private function dbGetInput()
    {

    }

    private function fileGetInput()
    {
        $data = null;
        if (file_exists('proxy_data/'.$this->_id)) {
            $data = file_get_contents('proxy_data/'.$this->_id);

            $data = explode("\n", $data);
            for ($i = 0; $i < count($data); $i++) {
                $data[$i] = base64_decode($data[$i]);
            }

            $data = implode("\n", $data);

            unlink('proxy_data/'.$this->_id);

            $this->webWriteDebug('Input at read: '.str_replace("\n", "\\\n", $data));
        }

        return $data;
    }

    private function generateId()
    {
        $str = '';
        $chars = str_split('abcdefghijklmnopqrstuvwxyz1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZ');

        for($i = 0; $i < 15; $i++) {
            $str .= $chars[rand(0, count($chars)-1)];
        }

        return $str;
    }
}

$t = new TelnetProxy();
$t->setTelnetInfo($_GET['h'], $_GET['p']);
$t->setInputType($t::INPUT_FILE);
$t->run();
?>
<script type="text/javascript">
    parent.mudclient.util.debugMsg('Webproxy read ended.', 'connection');
</script>
</body>
</html>