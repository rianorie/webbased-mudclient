class SocketBridge {

	static var socket;
	
	static var jsScope;
	static var PolicyPort;
	static var PolicyHost;

    static function main() {

        PolicyPort = flash.Lib.current.loaderInfo.parameters.policyPort;
        PolicyHost = flash.Lib.current.loaderInfo.parameters.policyHost;

        //flash.system.Security.loadPolicyFile("xmlsocket://"+PolicyHost+":"+PolicyPort);
        //trace("Loading Policy File from xmlsocket://"+PolicyHost+":"+PolicyPort);

        socket = new flash.net.Socket();
        trace("Socket created");

	    if (flash.external.ExternalInterface.available) {
	    
	    	jsScope = flash.Lib.current.loaderInfo.parameters.scope;
	    	
	    	if (jsScope == null) {
	    		jsScope = "";
	    	} else {
	    		jsScope += ".";
	    	}
	
			/* Calls the javascript load method once the SWF has loaded */
			//flash.external.ExternalInterface.call("setTimeout", jsScope + "loaded()");
            flash.external.ExternalInterface.call(jsScope + "loaded");
			
			// Set event listeners for socket
			
			// CONNECT
			socket.addEventListener(flash.events.Event.CONNECT, function(e) : Void { 
					trace("Connected to server");
					//flash.external.ExternalInterface.call("setTimeout", jsScope + "connected()", 0);
					flash.external.ExternalInterface.call(jsScope + "connected", "");
				}
			);

			// CLOSE
			socket.addEventListener(flash.events.Event.CLOSE, function(e) : Void {
					trace("Disconnected from server");
					//flash.external.ExternalInterface.call("setTimeout", jsScope + "disconnected()", 0);
                    flash.external.ExternalInterface.call(jsScope + "disconnected", "");
				}
			);
				

			// IO ERROR
			socket.addEventListener(flash.events.IOErrorEvent.IO_ERROR, function(e) : Void {
					trace("IOERROR : " +  e.text);
					//flash.external.ExternalInterface.call("setTimeout", jsScope + "ioError('" + e.text + "')" ,0);
					flash.external.ExternalInterface.call(jsScope + "ioError", e.text);

				}
			);

			// SECURITY ERROR
			socket.addEventListener(flash.events.SecurityErrorEvent.SECURITY_ERROR, function(e) : Void {
					trace("SECURITY ERROR : " +  e.text);
					//flash.external.ExternalInterface.call("setTimeout", jsScope + "securityError('" +e.text+ "')", 0);
					flash.external.ExternalInterface.call(jsScope + "securityError", e.text);
				}
			);

			// SOCKET DATA
			socket.addEventListener(flash.events.ProgressEvent.SOCKET_DATA, function(e) : Void {
						var msg:String = socket.readUTFBytes(socket.bytesAvailable);

						trace("Received : " + msg );

						//flash.external.ExternalInterface.call("setTimeout", jsScope + 'receive("' + msg + '")', 0);
						flash.external.ExternalInterface.call(jsScope + "receive", msg);
					}
				);
		
			/* Set External Interface Callbacks */
			
			// connect(host, port) 
			flash.external.ExternalInterface.addCallback("connect", connect);	    	
			
			// disconnect() 
			flash.external.ExternalInterface.addCallback("close", close);	    				
			
			// write() 
			flash.external.ExternalInterface.addCallback("write", write);

	    } else {
	    	trace("Flash external interface not available");
	    }   

    }
    
    /**
     * Connect to new socket server
     * @param host The host the socket server resides on
     * @param port The socket servers port
     */
    static function connect(host : String, port : String) {
    	trace("Connecting to socket server at " + host + ":" + port);
		socket.connect(host, Std.parseInt(port));    	
    }
    
    /**
     * Disconnect the socket
     */
    static function close() {
    	if (socket.connected) {
    	   	trace("Closing current connection");
			socket.close();
		} else {
			trace("Cannot disconnect to server because there is no connection!");
		}
    }

    /**
     * Write string to the socket
     */
    static function write(msg) {
    	if (socket.connected) {
	    	trace("Writing '" + msg + "' to server");
			socket.writeUTFBytes(msg);
			socket.flush();
		} else {
			trace("Cannot write to server because there is no connection!");		
		}
    }    

}