mudclient.modules = {

    _list : [],

    queue : [],

    init : function() {
        mudclient.util.debugMsg('Initiating modules.', 'modules');
        mudclient.util.debugMsg('Found '+this.queue.length+" modules to load.", 'modules');

        if (this.queue.length > 0) {
            for(var i in this.queue) {
                mudclient.util.addScript('modules/'+this.queue[i]+'.js');
            }
        }
    },

    load: function(sName, oModule) {

        mudclient.util.debugMsg('Loading module '+sName, 'modules');

        this._list[sName] = oModule;

        if (typeof this._list[sName].init == "function") {
            this._list[sName].init();
        } else {
            mudclient.error("Module "+sName+" failed to initiate.");
        }
    }
}
