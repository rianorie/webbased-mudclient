mudclient.connection = {

    host :'mudclient.localhost',
    port : 6401,

    policyHost: 'mudclient.localhost',
    policyPort: 5000,

    websocketHost : 'localhost',
    websocketPort : 5000,

    force : null,

    TYPE_JAVA      : 1,
    TYPE_FLASH     : 2,
    TYPE_WEBSOCKET : 3,
    TYPE_WEBPROXY  : 4,

    TYPE : null,

    test : function() {

        if (this.force != null) {
            mudclient.util.debugMsg("Using forced connection method.", "connection");
            this.TYPE = this.force;
            return this.TYPE;
        }

        mudclient.util.debugMsg('Testing available connection types.', 'connection');

        // We love websockets, they come first
        if (typeof WebSocket == "function") {
            mudclient.util.debugMsg('Websockets available.', 'connection');
            this.TYPE = this.TYPE_WEBSOCKET;
            return this.TYPE;
        }

        // Since flash wont complain that it needs permission
        // that comes next
        if (swfobject.getFlashPlayerVersion().major >= 7) {
            mudclient.util.debugMsg('Flashplayer available.', 'connection');
            this.TYPE = this.TYPE_FLASH;
            return this.TYPE;
        }

        // And finally, java
        if (navigator.javaEnabled()) {
            mudclient.util.debugMsg('Java available.', 'connection');
            this.TYPE = this.TYPE_JAVA;
            return this.TYPE;
        }

        mudclient.util.debugMsg("Resorting to webproxy", 'connection');
        this.TYPE = this.TYPE_WEBPROXY;
        return this.TYPE;

        // Throw a failure if none of the above made it through
//        mudclient.error("No suitable socket bridge available.");
//        mudclient.error("Either upgrade to a newer browser, or install flash or java.");
//        return false;
    },

    init : function() {
        mudclient.util.debugMsg('Initiating connection system.', 'connection');
        this.test();

        switch(this.TYPE) {
            case this.TYPE_JAVA:
                    this.adapters.java.setup();
                break;

            case this.TYPE_FLASH:
                    this.adapters.flash.setup();
                break;

            case this.TYPE_WEBSOCKET:
                    this.adapters.websocket.setup();
                break;

            case this.TYPE_WEBPROXY:
                    this.adapters.webproxy.setup();
                break;
        }
    },

    run : function() {
        mudclient.util.debugMsg('Running the connect function.', 'connection');
        this.connect(this.host, this.port);
    },

    adapters : {

        java : {
            ready : false,
            handle : null,

            blankPos : false,

            setup: function() {

                mudclient.util.debugMsg('Java adapter: setup', 'connection');

                // <applet id="JavaSocketBridge" archive="adapters/java/JavaSocketBridge.jar"
                // code="JavaSocketBridge.class" width="0" height="0"></applet>
                var el = document.createElement('applet');
                el.archive = "/adapters/java/JavaSocketBridge.jar";
                el.code = "JavaSocketBridge.class";
                el.width = 50;
                el.height = 50;

                this.handle = el;

                mudclient.connection.connect = this.connect;
                mudclient.connection.write   = this.write;

                jQuery('#connection_bridge_container')[0].appendChild(el);

                mudclient.msg("Mudclient loading...");
            },

            loaded : function() {
                window.setTimeout(function() {
                    mudclient.connection.run();
                }, 2000);
            },

            connect: function(sHost, iPort) {
                mudclient.util.debugMsg('Java adapter: connect', 'connection');

                if (!mudclient.connection.adapters.java.ready) {
                    mudclient.error('java.connect: Java bridge not ready yet');
                    return false;
                }
                return mudclient.connection.adapters.java.handle.connect(sHost, iPort);
            },

            disconnect: function() {
                mudclient.util.debugMsg('Java adapter: disconnect', 'connection');

                if (!mudclient.connection.adapters.java.ready) {
                    mudclient.error("java.disconnect: Java bridge not ready yet");
                    return false;
                }
                return mudclient.connection.adapters.java.handle.disconnect();
            },

            read: function(sTxt) {

                if (!mudclient.connection.adapters.java.ready) {
                    mudclient.error("java.read: Java bridge not ready yet");
                    return false;
                }

                // java socket bridge throws in blank empty lines between every
                // received line.
                if (sTxt == '') {
                    if (this.blankPos) {
                        this.blankPos = false;
                        mudclient.read( sTxt );
                    } else {
                        this.blankPos = true;
                    }
                } else {
                    this.blankPos = false;
                    mudclient.read( sTxt );
                }

            },

            write: function(sTxt) {
                if (!mudclient.connection.adapters.java.ready) {
                    mudclient.error("java.write: Java bridge not ready yet");
                    return false;
                }

                return mudclient.connection.adapters.java.handle.send(sTxt);
            },

            error: function(sTxt) {
                if (!mudclient.connection.adapters.java.ready) {
                    mudclient.error("java:error: Java bridge not ready yet");
                    return false;
                }

                mudclient.error(sTxt);
            }
        },

        flash : {

            handle : null,

            setup : function() {
                mudclient.util.debugMsg('Running setup for the flash adapter.', 'connection');

                var path = "adapters/flash/socket_bridge.swf";
                var so = new SWFObject(path, "connection_bridge", "50", "50", "9", "#ffffff");

                so.addParam("allowscriptaccess", "always");
                so.addVariable("scope", "mudclient.connection.adapters.flash");
                so.addVariable("policyHost", mudclient.connection.policyHost);
                so.addVariable("policyPort", mudclient.connection.policyPort);
                so.write("connection_bridge_container");

                this.handle = document.getElementById('connection_bridge');

                mudclient.connection.connect = this.connect;
                mudclient.connection.write   = this.write;
            },

            connect: function(sHost, iPort) {
                mudclient.util.debugMsg('flash adapter: connect.', 'connection');
                return mudclient.connection.adapters.flash.handle.connect(sHost, iPort);
            },

            disconnect: function() {
                mudclient.util.debugMsg('flash adapter: disconnect.', 'connection');
                return mudclient.connection.adapters.flash.handle.close();
            },

            read: function(sTxt) {
                if (sTxt[0] == "\n" || sTxt[0] == "\r") {
                    mudclient.read(sTxt.substr(1));
                } else {
                    mudclient.read(sTxt);
                }
            },

            write: function(sTxt) {
                return mudclient.connection.adapters.flash.handle.write(sTxt+"\n");
            },

            receive : function(sTxt) {

                var arrText = sTxt.split("\n");
                for(var i = 0; i < arrText.length; i++) {
                    this.read(arrText[i]);
                }
            },

            loaded: function() {
                mudclient.msg("Mudclient loading..");
                window.setTimeout(function() {
                    mudclient.connection.run();
                }, 1000);
            },

            connected: function() {
                mudclient.msg("Connected.");
            },

            disconnected: function() {
                mudclient.msg("Disconnected.");
            },

            ioError: function(sTxt) {
                mudclient.error("IoError: "+sTxt);
            },

            securityError: function(sTxt) {
                mudclient.error("SecurityError: "+sTxt);
            }
        },

        websocket : {
            handle : null,

            setup : function() {
                mudclient.util.debugMsg('Running setup for websocket adapter.', 'connection');
                this.handle = new WebSocket('ws://'+mudclient.connection.websocketHost+':'+mudclient.connection.websocketPort, ['base64']);

                this.handle.onopen    = this.connected;
                this.handle.onerror   = this.error;
                this.handle.onclose   = this.closed;
                this.handle.onmessage = this.receive;

                mudclient.connection.connect = this.connect;
                mudclient.connection.write   = this.write;
            },

            connect : function(sHost, iPort) {
                return true;
            },

            connected : function() {
                mudclient.msg("Connected.");
            },
            closed : function() {
                mudclient.msg("Disconnected.");
            },

            error : function(sTxt) {
                mudclient.error("An error occurred.");
            },

            write : function(sTxt) {
                sTxt = mudclient.util.base64_encode( sTxt+"\n" );
                mudclient.connection.adapters.websocket.handle.send( sTxt );
            },

            read : function(sTxt) {
                if (sTxt[0] == "\n" || sTxt[0] == "\r") {
                    mudclient.read(sTxt.substr(1));
                } else {
                    mudclient.read(sTxt);
                }
            },

            receive : function(event) {

                var sTxt = event.data;
                sTxt = mudclient.util.base64_decode( sTxt );

                var arrText = sTxt.split("\n");

                for(var i = 0; i < arrText.length; i++) {
                    mudclient.connection.adapters.websocket.read( arrText[i] );
                }
            }
        },

        webproxy : {

            id : null,

            webpath: 'adapters/webproxy/',

            setup : function() {

                jQuery('<iframe style="height: 50px; width: 50px;" id="connection_bridge"></iframe>').appendTo('#connection_bridge_container');
                jQuery('#connection_bridge').ready(function() {
                    mudclient.connection.run();
                });

                mudclient.connection.connect = this.connect;
                mudclient.connection.write = this.write;
            },

            connect : function(sHost, iPort) {

                var sURI = mudclient.connection.adapters.webproxy.webpath;
                sURI += 'proxy_read.php?h='+sHost+'&p='+iPort;

                mudclient.util.debugMsg('Webproxy at '+sURI, 'connection');

                jQuery('#connection_bridge').attr('src', sURI);
            },

            connected : function() {
                mudclient.util.debugMsg('Webproxy connected', 'connection');
            },

            disconnected : function() {
                mudclient.util.debugMsg('Webproxy disconnected', 'connection');
            },

            error : function(sTxt) {

                sTxt = mudclient.util.base64_decode( sTxt );
                var arrText = sTxt.split("\n");

                for(var i = 0; i < arrText.length; i++) {
                    mudclient.error( arrText[i] );
                }
            },

            write : function(sTxt) {

                sTxt = mudclient.util.base64_encode(sTxt);

                mudclient.util.debugMsg('Loading '+mudclient.connection.adapters.webproxy.webpath+'proxy_write.php?data='+sTxt+'&id='+mudclient.connection.adapters.webproxy.id, 'connection');
                jQuery.get(mudclient.connection.adapters.webproxy.webpath+'proxy_write.php?data='+sTxt+'&id='+mudclient.connection.adapters.webproxy.id);
            },

            read : function(sTxt) {
                if (sTxt[0] == "\n" || sTxt[0] == "\r") {
                    mudclient.read(sTxt.substr(1));
                } else {
                    mudclient.read(sTxt);
                }
            },

            receive : function(sTxt) {

                sTxt = mudclient.util.base64_decode( sTxt );
                var arrText = sTxt.split("\n");

                for(var i = 0; i < arrText.length; i++) {
                    mudclient.connection.adapters.webproxy.read( arrText[i] );
                }
            },

            setId : function(sId) {
                mudclient.connection.adapters.webproxy.id = sId;
                mudclient.util.debugMsg('Setting the id for the webproxy: '+sId, 'connection');
            }
        }
    }
}

// Java bridge
// Need to be replaced with proper hooks into the connection
function on_socket_get(sTxt) {
    mudclient.connection.adapters.java.read(sTxt);
}
function on_socket_error(sTxt) {
    mudclient.connection.adapters.java.error(sTxt);
}
function java_socket_bridge_ready(){
    mudclient.connection.adapters.java.ready = true;
    mudclient.connection.adapters.java.loaded();
}
