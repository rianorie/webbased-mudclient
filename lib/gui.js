mudclient.gui = {
    init : function() {
        jQuery(window).resize(function() {
            mudclient.gui.resize();
        });

        mudclient.gui.resize();

        jQuery('#buffer').click(function() {
            jQuery('#command').focus();
        });

        jQuery('#nav_help').click(function() {

            var str  = "WebMUD client v"+mudclient.version+"\n";
                str += "\n";
                str += "Author: Rian Orie\n";
                str += "License: to be determined.\n";
                str += "\n";
                str += "Loaded modules:\n";

            for(var i in mudclient.modules._list) {
                str += "  "+i+"\n";
            }

            alert(str);
        });
    },

    menu : {
        add : function(sLabel, sId, callback) {
            jQuery('<li><a id="'+sId+'" href="javascript:;">'+sLabel+'</a></li>').prependTo('#menu');
            jQuery('#'+sId).click(callback);
        }
    },

    dialog : {

        _title : null,
        _showadd : false,
        _onsave : null,
        _ondelete : null,
        _data : {containers : {}, items : {}, settings : {}},

        setTitle : function(sTxt) {
            this._title = sTxt;
        },

        showAdd : function(bShow) {
            this._showadd = (bShow == true);
        },

        setOnSave : function(callback) {
            this._onsave = callback;
        },

        setOnDelete : function(callback) {
            this._ondelete = callback;
        },

        addContainer : function(sLabel, sId) {
            this._data['containers'][sId] = {label:sLabel, id:sId};
        },

        addItem: function(sLabel, sId, oInfo, sContainerId) {
            this._data['items'][sId] = {label:sLabel, id:sId, data:oInfo, container:sContainerId};
        },

        addSetting : function(sLabel, sId, oInfo, sContainerId) {
            this._data['settings'][sId] = {label:sLabel, id:sId, data:oInfo, container:sContainerId};
        },

        show : function() {

            jQuery(mudclient).trigger('before_dialog_show');

            mudclient.util.debugMsg('Loading the gui dialog.', 'gui');

            jQuery.get('/dialog.html', function(data) {

                jQuery(data).appendTo(document.body);
                jQuery('#gui-dialog-close').click(mudclient.gui.dialog.hide);
                jQuery('#gui-dialog-label-data').html( mudclient.gui.dialog._title );
                if (mudclient.gui.dialog._showadd) {
                    jQuery('#gui-dialog-add').show();
                }

                var item;

                mudclient.gui.dialog.generateList();

                jQuery('#gui-dialog-add-item').click(function() {

                    jQuery('#gui-dialog-edit-notification').hide();
                    jQuery('#gui-dialog-edit-settings').hide();
                    jQuery('#gui-dialog-edit-container').hide();

                    var container = '';
                    if (jQuery('#gui-dialog-list a.active').length > 0) {
                        var jThis = jQuery('#gui-dialog-list a.active:first');
                        var item = mudclient.gui.dialog._data[jThis.attr('type')+'s'][jThis.attr('rel')];

                        if (item.type == 'container') {
                            container = item.id;
                        } else {
                            container = item.container;
                        }
                    }

                    jQuery('#gui-dialog-edit-item-id').val(mudclient.gui.dialog.generateId());
                    jQuery('#gui-dialog-edit-item-container').val( container );
                    jQuery('#gui-dialog-edit-item-name').val('');
                    jQuery('#gui-dialog-edit-item-pattern').val('');
                    jQuery('#gui-dialog-edit-item-data').val('');
                    jQuery('#gui-dialog-edit-item-regex').prop('checked', false);
                    jQuery('#gui-dialog-edit-item').show();
                });

                jQuery('#gui-dialog-edit-item-delete').click(function() {
                    jQuery('#gui-dialog-edit-item').hide();
                    jQuery('#gui-dialog-edit-notification').html('Item removed!').attr('class', 'notification-success').show();

                    var sId = jQuery('#gui-dialog-edit-item-id').val();
                    mudclient.gui.dialog._ondelete(sId);
                    delete mudclient.gui.dialog._data.items[sId];
                    mudclient.gui.dialog.generateList();
                });

                jQuery('#gui-dialog-edit-item-save').click(function() {
                    var sId = jQuery('#gui-dialog-edit-item-id').val();
                    jQuery('#gui-dialog-edit-notification').hide();

                    var newdata = jQuery('#gui-dialog-edit-item-data').val();
                    var newpattern = jQuery('#gui-dialog-edit-item-pattern').val();

                    if (jQuery('#gui-dialog-edit-item-regex').prop('checked')) {
                        try {
                            eval('/'+newpattern+'/.test("mudclient");');
                        } catch(e) {
                            mudclient.util.debugMsg("Error in pattern while saving item: "+e);
                            jQuery('#gui-dialog-edit-notification').html('Item not saved, there\'s an error in the pattern.<br />'+e).attr('class', 'notification-error').show();
                            return;
                        }
                    }

                    try {
                        var tmp = newdata;
                        tmp = tmp.replace(/send/g, 'dummy.send')
                                 .replace(/echo/g, 'dummy.echo')
                                 .replace(/mudclient\.write/g, 'dummy.send')
                                 .replace(/mudclient\.msg/g, 'dummy.echo')
                                 .replace(/'/g, '\\\'');

                        eval('(function() {'+tmp+'})');

                        item = {
                            id: sId,
                            enabled : true,
                            type: 'item',
                            container : jQuery('#gui-dialog-edit-item-container').val(),
                            name : jQuery('#gui-dialog-edit-item-name').val(),
                            pattern : jQuery('#gui-dialog-edit-item-pattern').val(),
                            data : newdata,
                            regex : jQuery('#gui-dialog-edit-item-regex').prop('checked')
                        };

                        mudclient.gui.dialog._onsave(sId, item);
                        mudclient.gui.dialog._data.items[sId] = {id:sId, label:item.name, container:item.container, data:item};
                        mudclient.gui.dialog.generateList();
                        jQuery('#item_'+sId+' a').addClass('active');

                        jQuery('#gui-dialog-edit-notification').html('Item saved!').attr('class', 'notification-success').show();

                    } catch(e) {
                        mudclient.util.debugMsg("Error while saving item: "+e);
                        jQuery('#gui-dialog-edit-notification').html('Item not saved, there\'s an error in the script.<br />'+e).attr('class', 'notification-error').show();
                    }
                });


                jQuery('#gui-dialog-add-container').click(function() {

                    jQuery('#gui-dialog-edit-notification').hide();
                    jQuery('#gui-dialog-edit-settings').hide();
                    jQuery('#gui-dialog-edit-item').hide();

                    jQuery('#gui-dialog-edit-container-id').val(mudclient.gui.dialog.generateId());
                    jQuery('#gui-dialog-edit-container-name').val('');
                    jQuery('#gui-dialog-edit-container').show();
                });

                jQuery('#gui-dialog-edit-container-delete').click(function() {

                    var sId = jQuery('#gui-dialog-edit-container-id').val();

                    var count = 0;
                    for(var i in mudclient.gui.dialog._data.items) {
                        if (mudclient.gui.dialog._data.items[i].container == sId) {
                            count++;
                        }
                    }

                    if (count > 0) {
                        if (!confirm("There "+(count == 1 ? 'is one item' : "are a total of "+count+" items")+" in this container.\n"+(count == 1 ? 'It' : 'They')+" will be removed along with the container.\nContinue?")) {
                            return;
                        }
                    }

                    jQuery('#gui-dialog-edit-container').hide();
                    jQuery('#gui-dialog-edit-notification').html('Container removed!').attr('class', 'notification-success').show();


                    mudclient.gui.dialog._ondelete(sId);
                    delete mudclient.gui.dialog._data.containers[sId];
                    mudclient.gui.dialog.generateList();
                });

                jQuery('#gui-dialog-edit-container-save').click(function() {
                    var sId = jQuery('#gui-dialog-edit-container-id').val();
                    item = {
                        id: sId,
                        type: 'container',
                        name : jQuery('#gui-dialog-edit-container-name').val()
                    };

                    mudclient.gui.dialog._onsave(sId, item);
                    mudclient.gui.dialog._data.containers[sId] = item;
                    mudclient.gui.dialog.generateList();
                    jQuery('#container_'+sId+' a').addClass('active');

                    jQuery('#gui-dialog-edit-notification').html('Item saved!').attr('class', 'notification-success').show();
                });
            });

            jQuery(mudclient).trigger('after_dialog_show');
        },

        generateList : function() {
            var i, eHtml, item;

            mudclient.util.debugMsg('Generating the list of elements in the gui dialog.', 'gui');

            jQuery('#gui-dialog-list').html('');

            for(i in mudclient.gui.dialog._data.containers) {
                eHtml = jQuery('#gui-dialog-template-container').html();
                eHtml = eHtml.replace('##title##', mudclient.gui.dialog._data.containers[i].name);
                eHtml = eHtml.replace('##rel##', mudclient.gui.dialog._data.containers[i].id);
                jQuery('<li id="container_'+ mudclient.gui.dialog._data.containers[i].id+'">'+eHtml+'</li>').appendTo('#gui-dialog-list');
            }

            for(i in mudclient.gui.dialog._data.items) {
                item = mudclient.gui.dialog._data.items[i];

                eHtml = jQuery('#gui-dialog-template-item').html();
                eHtml = eHtml.replace(/##title##/g, item.label).replace(/##rel##/g, item.id);

                if (item.container) {
                    jQuery('<li id="item_'+ item.id+'">'+eHtml+'</li>').appendTo('#container_'+ item.container+' ul');
                } else {
                    jQuery('<li id="item_'+ item.id+'">'+eHtml+'</li>').prependTo('#gui-dialog-list');
                }

                if (item.data.enabled) {
                    jQuery('#item_'+item.id+' input').prop('checked', true);
                }
            }

            for( i in mudclient.gui.dialog._data.settings) {
                item = mudclient.gui.dialog._data.settings[i];

                eHtml = jQuery('#gui-dialog-template-setting').html();
                eHtml = eHtml.replace(/##title##/g, item.label).replace(/##rel##/g, item.id);

                if (item.container) {
                    jQuery('<li id="item_'+ item.id+'">'+eHtml+'</li>').appendTo('#container_'+ item.container+' ul');
                } else {
                    jQuery('<li id="item_'+ item.id+'">'+eHtml+'</li>').prependTo('#gui-dialog-list');
                }
            }

            jQuery('#gui-dialog-list a').on('click', function() {
                var jThis = jQuery(this);
                var item = mudclient.gui.dialog._data[jThis.attr('type')+'s'][jThis.attr('rel')];

                jQuery('#gui-dialog-list a').each(function() { jQuery(this).removeClass('active'); });
                jThis.addClass('active');

                if (jThis.attr('type') == 'item') {

                    jQuery('#gui-dialog-edit-settings').hide();
                    jQuery('#gui-dialog-edit-container').hide();
                    jQuery('#gui-dialog-edit-notification').hide();

                    jQuery('#gui-dialog-edit-item-id').val( item.id );
                    jQuery('#gui-dialog-edit-item-container').val( item.container );
                    jQuery('#gui-dialog-edit-item-name').val( item.data.name );
                    jQuery('#gui-dialog-edit-item-pattern').val( item.data.pattern );
                    jQuery('#gui-dialog-edit-item-data').val( item.data.data );
                    jQuery('#gui-dialog-edit-item-regex').prop('checked', item.data.regex);
                    jQuery('#gui-dialog-edit-item').show();

                } else if (jThis.attr('type') == 'setting') {

                    jQuery('#gui-dialog-edit-settings-container').html('');
                    jQuery('#gui-dialog-edit-item').hide();
                    jQuery('#gui-dialog-edit-container').hide();
                    jQuery('#gui-dialog-edit-notification').hide();

                    for(i = 0; i < item.data.settings.length; i++) {

                        var setting = item.data.settings[i];

                        var eHtml = jQuery('#edit-settings-template').clone().removeAttr('id');
                        jQuery('label', eHtml).html(setting.label);
                        jQuery('select', eHtml).attr('rel', i);

                        // boolean
                        if (setting.type == 1) { setting.options = {0:'No', 1:'Yes'}; }

                        // Add options
                        for (var val in setting.options) {
                            jQuery('<option value="'+val+'"'+(setting.value == val ? ' selected="selected"' : '')+'>'+setting.options[val]+'</option>').appendTo(jQuery('select', eHtml));
                        }

                        jQuery(eHtml).appendTo('#gui-dialog-edit-settings-container');
                    }

                    jQuery('#gui-dialog-edit-settings').show();


                } else if (jThis.attr('type') == 'container') {

                    jQuery('#gui-dialog-edit-settings').hide();
                    jQuery('#gui-dialog-edit-item').hide();
                    jQuery('#gui-dialog-edit-notification').hide();

                    jQuery('#gui-dialog-edit-container-id').val( item.id );
                    jQuery('#gui-dialog-edit-container-name').val( item.name );
                    jQuery('#gui-dialog-edit-container').show();

                }
            });
        },

        hide : function() {
            jQuery(mudclient).trigger('before_dialog_hide');
            jQuery('#gui-dialog').remove();
            jQuery(mudclient).trigger('after_dialog_hide');
            mudclient.gui.dialog.cleanData();
            jQuery(mudclient).trigger('after_dialog_cleandata');
        },

        cleanData : function() {
            this._showadd = false;
            this._title = null;
            this._data = {containers : {}, items : {}, settings : {}};
        },

        generateId : function() {
            var id = 'xxxxxxxx'.replace(/[xy]/g, function(c) {
                var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
                return v.toString(16);
            });

            return id;
        }
    },

    resize : function() {
        var iFormHeight = jQuery('#entryform').height();
        var iBodyHeight = jQuery('body').height();
        jQuery('#buffer').height( iBodyHeight-iFormHeight-5-21 ); // -5 for padding, -21 for header navigation

        jQuery('#command').width( jQuery('#buffer').width()-4 );
    }
}