mudclient.data = {

    _store : {},

    get : function(sKey) {
        if (this._store[sKey]) {
            return this._store[sKey];
        }

        return null;
    },

    set : function(sKey, mData) {
        this._store[sKey] = mData;
    }
}