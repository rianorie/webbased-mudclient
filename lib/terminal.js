mudclient.terminal = {

    _linewrap : 80,

    init : function() {

    },

    line : {
        add: function(sTxt) {
            var line = mudclient.terminal.util.getLineHtml(sTxt);
            var out = jQuery(line).appendTo('#buffer');
            mudclient.terminal.util.setScroll();

            return out;
        },

        get : function(iPos) {

        },

        read : function(iPos) {

        }
    },

    error : {
        add : function(sTxt) {
            var line = mudclient.terminal.util.getLineHtml(sTxt, 'error');
            var out = jQuery(line).appendTo('#buffer');
            mudclient.terminal.util.setScroll();

            return out;
        }
    },

    cmd : {
        add: function(sTxt) {

            var lines = mudclient.terminal.util.splitLine(sTxt);

            for(var i = 0; i < lines.length; i++) {
                var line = mudclient.terminal.util.getLineHtml(lines[i], 'cmd');
                jQuery(line).appendTo('#buffer');
                mudclient.terminal.util.setScroll();
            }
        }
    },

    msg : {
        add: function(sTxt) {
            var line = mudclient.terminal.util.getLineHtml(sTxt, 'msg');
            var out = jQuery(line).appendTo('#buffer');
            mudclient.terminal.util.setScroll();

            return out;
        }
    },

    util : {
        setScroll : function() {
            document.getElementById('buffer').scrollTop = document.getElementById('buffer').scrollHeight;
        },

        splitLine : function(sTxt) {

            var len=mudclient.terminal._linewrap, curr = len, prev = 0;
            var output = [];

            while(sTxt[curr]) {
                if(sTxt[curr++] == ' ') {
                    output.push(sTxt.substring(prev,curr));
                    prev = curr;
                    curr += len;
                }
            }

            output.push(sTxt.substr(prev));

            return output;
        },

        getTimestamp : function() {
            var _curTime = new Date();

            var out = _curTime.getFullYear();
            out += "-";
            out += this._numberpad(_curTime.getMonth(), 2);
            out += "-";
            out += this._numberpad(_curTime.getDay(), 2);
            out += " ";
            out += this._numberpad(_curTime.getHours(), 2);
            out += ":";
            out += this._numberpad(_curTime.getMinutes(), 2);
            out += ":";
            out += this._numberpad(_curTime.getSeconds(), 2);
            out += ".";
            out += _curTime.getMilliseconds();

            return out;
        },

        getLineHtml : function(sTxt, sClass) {

            if (!sClass) { sClass = ''; }
            else         { sClass = ' '+sClass; }

            //if (/^\s*$/.test(sTxt)) { console.log( sTxt.replace("\n", '\n').replace("\r", '\r')+" - "+sTxt.length ); }
            if (sTxt == '') { sTxt = '&nbsp;'; }

            return '<span class="line'+sClass+'"><span class="ts">'+this.getTimestamp()+'</span><span class="data">'+sTxt+'</span></span>';
        },

        _numberpad : function (a,b) {
            return (1e15+a+"").slice(-b);
        }
    }
}