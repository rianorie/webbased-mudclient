mudclient = {

    version: 0.1,

    debug : true,

    init : function() {

        mudclient.connection.force = mudclient.connection.TYPE_WEBPROXY;

        mudclient.terminal.init();
        mudclient.gui.init();
        mudclient.connection.init();
        mudclient.modules.init();

        jQuery(mudclient).trigger("on_init");

//        window.setTimeout(mudclient.connection.run, 1000);
    },


    read : function(sTxt) {

        // We need an object in order to pass the string as a reference
        var obj = {string : sTxt};

        jQuery(mudclient).trigger('on_read', obj);
        mudclient.terminal.line.add( obj.string );
    },

    msg : function(sTxt) {
        // We need an object in order to pass the string as a reference
        var obj = {string : sTxt};

        jQuery(mudclient).trigger('on_msg', obj);
        mudclient.terminal.msg.add( obj.string );
    },

    error : function(sTxt) {
        // We need an object in order to pass the string as a reference
        var obj = {string : sTxt};

        jQuery(mudclient).trigger('on_error', obj);
        mudclient.terminal.error.add( obj.string );
    },

    write : function(sTxt, bEvent) {
        // We need an object in order to pass the string as a reference
        var obj = {string : sTxt};

        if (bEvent == null || bEvent == true) {
            jQuery(mudclient).trigger('on_write', obj);
        }

        mudclient.terminal.cmd.add( obj.string );
        mudclient.connection.write( obj.string );
    }
}

// Wrappers for faster and easier scripting
function send(sTxt) { mudclient.write(sTxt, false); }
function echo(sTxt) { mudclient.msg(sTxt); }

dummy = {
    send : function(sTxt) { },
    echo : function(sTxt) { }
}