<!DOCTYPE html>
<html>
<head>
    <title>HTML MUD Client</title>
    <meta charset="utf-8" />
    <script type="text/javascript" src="lib/mudclient.js"></script>
    <script type="text/javascript" src="lib/data.js"></script>
    <script type="text/javascript" src="lib/utilities.js"></script>
    <script type="text/javascript" src="lib/gui.js"></script>
    <script type="text/javascript" src="lib/terminal.js"></script>
    <script type="text/javascript" src="lib/connection.js"></script>
    <script type="text/javascript" src="lib/modules.js"></script>

    <script type="text/javascript" src="jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="adapters/flash/swfobject.js"></script>
    <script type="text/javascript" src="adapters/flash/swfobject-2.2.js"></script>

    <link href="://fonts.googleapis.com/css?family=Droid+Sans+Mono" rel="stylesheet" type="text/css" />
    <link href="base.css" media="screen" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        mudclient.modules.queue = <?php
        $plugins = scandir(dirname(__FILE__).'/modules/');
        unset($plugins[0], $plugins[1]);
        $out = array();
        foreach($plugins as $plugin) { $out[] = str_replace('.js', '', $plugin); }
        echo json_encode($out);
        ?>;

        jQuery(document).ready(function() {
            jQuery('#entryform').submit(function(event) {
                event.stopPropagation();

                mudclient.write(jQuery("#command").val());
                jQuery('#command').focus().select();

                return false;
            });

            jQuery('#command').focus();

            mudclient.init();
        });

    </script>

</head>
<body>
<nav>
    <ul id="menu">
        <li class="last"><a href="javascript:;" id="nav_help">Info</a></li>
    </ul>
</nav>
<pre id="buffer">

</pre>
<form id="entryform" autocomplete="off">
    <label>
        <input type="text" id="command" autocomplete="off" name="command_<?=rand(1000, 9999);?>" />
    </label>
</form>


<div id="connection_bridge_container"></div>
</body>
</html>
