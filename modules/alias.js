aliasModule = {
    init : function() {
        mudclient.gui.menu.add('Aliases', 'alias', function() {

            mudclient.gui.dialog.hide();

            mudclient.gui.dialog.setTitle('Aliases');
            mudclient.gui.dialog.showAdd(true);
            mudclient.gui.dialog.setOnSave(function(s, d) { aliasModule.setData(s, d); });
            mudclient.gui.dialog.setOnDelete(function(s)  { aliasModule.removeData(s); });

            var elements = aliasModule.getAllData();
            for(var i in elements) {
                if (elements[i].type == 'container') {
                    mudclient.gui.dialog.addContainer(elements[i].name, elements[i].id);
                } else if (elements[i].type == 'item') {
                    mudclient.gui.dialog.addItem(elements[i].name, elements[i].id, elements[i], elements[i].container);
                }
            }

            mudclient.gui.dialog.show();
        });

        jQuery(mudclient).on('on_write', function(e, oInput) {

            var aliases = aliasModule.getAllData();

            for(var i in aliases) {
                if (aliases[i].enabled) {
                    if (aliases[i].regex) {
                        var regex = new RegExp(aliases[i].pattern);
                        if (regex.test(oInput.string)) {
                            mudclient.util.debugMsg('Regular expression '+aliases[i].pattern+' matched '+oInput.string+' for alias '+aliases[i].name, 'alias');

                            mudclient.terminal.cmd(oInput.string);
                            oInput.string = '';

                            var matches = oInput.string.match(regex);
                            mudclient.util.debugMsg('(function(matches) {'+aliases[i].data.replace(/'/g, '\\\'')+'}(matches))', 'alias');
                            eval('(function(matches) {'+aliases[i].data.replace(/'/g, '\\\'')+'}(matches))');

                        }
                    } else {
                        if (aliases[i].pattern == oInput.string) {
                            mudclient.util.debugMsg('Simple alias '+aliases[i].pattern+' matched '+oInput.string+' for alias '+aliases[i].name, 'alias');

                            mudclient.terminal.cmd(oInput.string);
                            oInput.string = '';

                            eval('(function(){'+aliases[i].data.replace(/'/g, '\\\'')+'}())');
                        }
                    }
                } else {
                    mudclient.util.debugMsg('Alias '+i+' is disabled.', 'alias');
                }
            }
        });
    },

    getAllData : function() {
        var tmp = mudclient.data.get('alias');
        if (tmp == null) {tmp = {}}
        return tmp;
    },

    getData : function(sKey) {
        var aliases = mudclient.data.get('alias');
        if (aliases == null) { aliases = {}; }
        if (aliases[sKey]) {
            return aliases[sKey];
        }

        return null;
    },

    setData : function(sKey, oData) {
        var aliases = mudclient.data.get('alias');
        if (aliases == null) { aliases = {}; }
        aliases[sKey] = oData;
        return mudclient.data.set('alias', aliases);
    },

    removeData : function(sKey) {
        var aliases = mudclient.data.get('alias');
        if (aliases == null) { aliases = {}; }
        if (aliases[sKey]) {
            delete aliases[sKey];
        }
        return mudclient.data.set('alias', aliases);
    }
}

mudclient.modules.load('alias', aliasModule);