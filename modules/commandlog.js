commandLog = {

    _log : [],
    _pos : 0,

    _limit : 20,

    init : function() {
        jQuery(mudclient).on("on_write", function(e, o) {
            if (o.string != commandLog._log[0]) {
                commandLog._log.unshift(o.string);
                commandLog._log = commandLog._log.slice(0, commandLog._limit);
            }
            commandLog._pos = 0;
        });

        jQuery('#command').keydown(function(event) {
            // Key up
            if (event.which == 38) {
                event.preventDefault();

                commandLog._pos++;
                if (commandLog._log[commandLog._pos]) {
                    jQuery('#command').val( commandLog._log[commandLog._pos]).focus().select();
                }

            // Key down
            } else if (event.which == 40) {
                event.preventDefault();

                if (commandLog._pos > 0) { commandLog._pos--; }
                if (commandLog._log[commandLog._pos]) {
                    jQuery('#command').val( commandLog._log[commandLog._pos]).focus().select();

                }
            }
        });
    }
}

mudclient.modules.load("commandlog", commandLog);