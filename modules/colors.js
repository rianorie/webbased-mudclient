var colorModule = {

    _map : {
            "0;31m"	: "red",           "0;32m" : "green",       "0;33m" : "yellow",        "0;34m" : "blue",
            "0;35m" : "purple",        "0;36m" : "cyan",        "0;37m" : "lightgrey",     "1;30m" : "darkgrey",
            "1;31m" : "brightred",     "1;32m" : "brightgreen", "1;33m" : "brightyellow",  "1;34m" : "brightblue",
            "1;35m" : "brightpurple",  "1;36m" : "brightcyan",  "1;37m" : "white"
    },

    _lastColor : null,

    init : function() {
        jQuery(mudclient).on('on_read', function(e, oTxt) {
            oTxt.string = colorModule.applyColors(oTxt.string);
        });
    },

    applyColors : function(sTxt) {

        var colorStart = String.fromCharCode(27, 91);
        var colorized = false;

        var full, code;

        while (sTxt.indexOf(colorStart) > -1) {

            if (/38;5;([0-9]+)m/.test(sTxt.substr(sTxt.indexOf(colorStart)+2, 9))) {

                full  = sTxt.substr(sTxt.indexOf(colorStart), 11);
                code  = sTxt.substr(sTxt.indexOf(colorStart)+2, 9).match(/5;(.+)m/);
                code = code[1];

                sTxt = sTxt.replace(full, '</span><span class="color_'+code+'">');
                colorized = true;

                colorModule._lastColor = code;

            } else if (/(0|1);([0-9]+)m/.test(sTxt.substr(sTxt.indexOf(colorStart)+2, 5))) {

                full  = sTxt.substr(sTxt.indexOf(colorStart), 7);
                code  = sTxt.substr(sTxt.indexOf(colorStart)+2, 5);

                if (colorModule._map[code]) {
                    sTxt = sTxt.replace(full, '</span><span class="color_'+colorModule._map[code]+'">');
                    colorized = true;

                    colorModule._lastColor = colorModule._map[code];
                }
            }
        }

        sTxt = '<span class="color_'+colorModule._lastColor+'">'+sTxt+'</span>';

        return sTxt;
    }
}

mudclient.modules.load('color', colorModule);