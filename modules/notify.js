notifyModule = {

    _title : null,

    _count : 0,

    init : function() {

        jQuery(window).focus(function() {
            jQuery(mudclient).off('on_read', notifyModule.updateTitle);

            notifyModule._count = 0;
            document.title = notifyModule._title;

        }).blur(function() {
                if (notifyModule._title == null) {
                    notifyModule._title = document.title;
                }
            jQuery(mudclient).on('on_read', notifyModule.updateTitle);
        });
    },

    updateTitle : function() {
        notifyModule._count++;
        document.title = '('+notifyModule._count+') '+notifyModule._title;
    }
}

mudclient.modules.load('notifier', notifyModule);